﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumSample.PageObjects
{
    public class HomePage
    {
        private IWebDriver driver;
        private By searchFieldLocator = By.Id("search_from_str");
        private By newsLinkLocator = By.LinkText("Новости TUT.BY");
        private By searchButton = By.Name("search");

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        internal void SelectSearchField()
        {
            driver.FindElement(searchFieldLocator).Click();
        }

        internal void SelectNewsSearch()
        {
            driver.FindElement(newsLinkLocator).Click();
        }

        internal SearchResultsPage Search(string searchText)
        {
            driver.FindElement(searchFieldLocator).SendKeys(searchText);
            driver.FindElement(searchButton).Click();

            return new SearchResultsPage(driver);
        }
    }
}
