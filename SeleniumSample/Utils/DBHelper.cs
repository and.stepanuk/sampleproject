﻿using System.Data;
using System.Data.SqlClient;

namespace SeleniumSample
{
    public class DBHelper
    {
        private string _connectionString;

        public DBHelper(string serverName, string databaseName)
        {
            _connectionString = $"Server={serverName};DataBase={databaseName};Integrated Security=SSPI";
        }

        public DBHelper(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void AddPerson(string firstName, string lastname, string storedProcedureName, string nameOfTheFirstParameterInSP, string nameOfTheSecondParameterInSP)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand(storedProcedureName, conn);

                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter($"@{nameOfTheFirstParameterInSP}", firstName));
                cmd.Parameters.Add(new SqlParameter($"@{nameOfTheSecondParameterInSP}", lastname));

                // execute the command
                cmd.ExecuteNonQuery();
            }
        }

        public int GetUserId(string firstName, string lastName)
        {
            int id;
            string sql =
                $"SELECT TOP 1 BusinessEntityID FROM Person.Person where FirstName = '{firstName}' and LastName = '{lastName}'";
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                id = (int)cmd.ExecuteScalar();
            }
            return id;
        }
    }
}
