using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;
using SeleniumSample.PageObjects;

namespace SeleniumSample
{
    [TestFixture]
    public class SeleniumTests
    {
        [SetUp]
        public void SetUp()
        {
            Site.InitDriver();
        }

        [TearDown]
        protected void TearDown()
        {
            Site.CloseDriver();
        }

        [Test]
        public void VerifyNews()
        {
            string homePage = "http://tut.by";
            string searchText = "EPAM Systems";

            var tutHomePage = Site.OpenPage(homePage);
            tutHomePage.SelectSearchField();
            tutHomePage.SelectNewsSearch();
            
            var searchResultPage = tutHomePage.Search(searchText);

            Assert.That(searchResultPage.ResultKeyword, Is.EqualTo("�EPAM Systems�"));
        }
    }
}