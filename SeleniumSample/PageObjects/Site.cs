﻿using OpenQA.Selenium;
using SeleniumSample.Drivers;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumSample.PageObjects
{
    public static class Site
    {
        public static IWebDriver driver;

        public static void InitDriver()
        {
            driver = DriverCreator.GetDriver();
        }

        public static HomePage OpenPage(string pageUrl)
        {
            driver.Navigate().GoToUrl(pageUrl);
            return new HomePage(driver);
        }

        internal static void CloseDriver()
        {
            driver.Quit();
        }
    }
}
