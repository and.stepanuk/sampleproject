﻿using NUnit.Framework;

namespace SeleniumSample
{
    [TestFixture]
    public class DBTests
    {
        [Ignore("For last lection homework")]
        [Test]
        public void DBTest()
        {
            #region data
            string firstName = "Joe";
            string lastName = "Doe";
            string serverName = "SERVERNAME\\SQLEXPRESS";
            string databaseName = "AdventureWorks2017";
            string storedProcedureName = "dbo.AddingNewEmployee2";
            string nameOfTheFirstParameterInSP = "FirstName";
            string nameOfTheSecondParameterInSP = "LastName";
            #endregion

            var db = new DBHelper(serverName, databaseName);
            db.AddPerson(firstName, lastName, storedProcedureName, nameOfTheFirstParameterInSP, nameOfTheSecondParameterInSP);
            var userId = db.GetUserId(firstName, lastName);
            Assert.AreNotEqual(default(int), userId);
        }
    }
}
